Rails.application.routes.draw do
  devise_for :users, :path_prefix => 'd'

  resources :users, only: [:show,:index,:destroy] do
     member do
        get :following, :followers
     end
  end
  root to: 'static_pages#home'
  get  '/about',   to: 'static_pages#about'
  resources :microposts, only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

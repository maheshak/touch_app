class UsersController < ApplicationController
   before_action :logged_in_user, only: :destroy
  
   def index
      @users = User.paginate(page: params[:page])
   end
   def show
      @user = User.find_by_username(params[:id])
      if user_signed_in? and policy(@user).show? 
        @micropost = current_user.microposts.build 
      end
      @microposts = @user.microposts.paginate(page: params[:page])

   end

   def destroy
      @user = User.find_by_username(params[:id])
      authorize @user 
      User.find_by_username(params[:id]).destroy
      flash[:success] = "User deleted"
      redirect_to users_url
   end

  def following
    @title = "Following"
    @user  = User.find_by_username(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find_by_username(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  
end

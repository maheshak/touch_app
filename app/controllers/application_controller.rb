class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

    protected

      def configure_permitted_parameters
          added_attrs = [:name, :username, :email, :password, :password_confirmation, :remember_me]
	  devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
	  devise_parameter_sanitizer.permit :account_update, keys: added_attrs
      end

  def after_sign_in_path_for(resource)
     root_url
  end
  def after_sign_in_path_for(root_path)
     root_url
  end
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    flash[:danger] = "You are not authorized to perform this action."
    redirect_to(request.referer||root_path)
  end
  def logged_in_user
     unless user_signed_in?
        flash[:danger] = "Please log in."
        redirect_to new_user_session_path 
      end
  end


end

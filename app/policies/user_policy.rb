class UserPolicy 
   attr_reader :current_user, :user, :signed_in

   def initialize(current_user, user)
      @current_user = current_user
      @user = user
   end

   def show?
      @current_user == @user
   end

   def destroy?
      @current_user.admin? and @current_user != @user
   end


end

class MicropostPolicy
  attr_reader :current_user, :micropost

  def initialize(current_user,micropost)
     @current_user = current_user
     @micropost = micropost
  end

  def destroy?
    @current_user == @micropost.user 

  end
end
